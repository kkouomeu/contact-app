package com.app.demo.appdemo.dao;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.demo.appdemo.entities.Contact;

public interface ContactRepository extends JpaRepository<Contact, Long> {
	
	/* Search contact per page */
	@Query(
			value="SELECT * FROM Contact WHERE firstname LIKE :x OR lastname LIKE :x",
			countQuery="SELECT count(*) AS 'totalContact' FROM Contact",
			nativeQuery = true
			)
	public Page<Contact> findContact(@Param("x")String keyWord, Pageable pageable);
	
}
