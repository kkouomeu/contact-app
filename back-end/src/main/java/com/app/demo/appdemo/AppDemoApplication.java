package com.app.demo.appdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.app.demo.appdemo.dao.ContactRepository;
import com.app.demo.appdemo.entities.Contact;

@SpringBootApplication
public class AppDemoApplication implements CommandLineRunner {

	@Autowired
	private ContactRepository contactRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(AppDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		contactRepository.save(new Contact("hamed", "martial", "12/06/2003", "h.mart@gmail.com", 633449021, "photo.jpeg"));
		contactRepository.save(new Contact("John", "bradley", "12/06/2003", "j.brad@gmail.com", 678679021, "photo2.jpeg"));
		contactRepository.save(new Contact("matt", "garcia", "12/06/2003", "matt.garcia@gmail.com", 635555021, "photo3.jpeg"));
		contactRepository.save(new Contact("Lucas", "Boris", "12/06/2003", "h.mart@gmail.com", 633449021, "photo.jpeg"));
		contactRepository.save(new Contact("Tom", "Jdudds", "12/06/2003", "j.brad@gmail.com", 678679021, "photo2.jpeg"));
		
		contactRepository.findAll().forEach(c->{
			System.out.println(c.getFirstname());
		});
	}
}
