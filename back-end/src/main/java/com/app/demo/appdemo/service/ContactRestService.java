package com.app.demo.appdemo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.demo.appdemo.dao.ContactRepository;
import com.app.demo.appdemo.entities.Contact;



@RestController
@CrossOrigin("*")
public class ContactRestService {
	
	@Autowired
	private ContactRepository contactRepository;
	
	
	//GET METHODS
	@GetMapping("/contacts")
	public List<Contact> getContacts() {
		return contactRepository.findAll();
	}

	/* This method has been defined in Interface ContactRepository */
	@GetMapping("/findContacts")
	public Page<Contact> find(@RequestParam(name="keyword", defaultValue="") String keyWord, 
							  @RequestParam(name="page", defaultValue="0") int page,
							  @RequestParam(name="size", defaultValue="5") int size) {
		return contactRepository.findContact("%"+keyWord+"%", new PageRequest(page, size));
	}
	
	@GetMapping("/contacts/{id}")
	public Optional<Contact> getContact(@PathVariable Long id) {
		return contactRepository.findById(id);
	}
	
	
	//POST METHODS
	@PostMapping("/contacts")
	public Contact save(@RequestBody Contact c) {
		return contactRepository.save(c);
	}
	
	
	//DELETE METHODS
	@DeleteMapping("/contacts/{id}")
	public boolean deleteContact(@PathVariable Long id) {
		contactRepository.deleteById(id);
		return true;
	}
	
	
	//PUT METHODS
	@PutMapping("/contacts/{id}")
	public Contact save(@PathVariable Long id,@RequestBody Contact c) {
		c.setId(id);
		return contactRepository.save(c);
	}

}
