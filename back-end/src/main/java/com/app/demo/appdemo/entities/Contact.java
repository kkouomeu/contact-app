package com.app.demo.appdemo.entities;

import java.io.Serializable;

import javax.persistence.*;

/* Les entités sont sérialisable */
@SuppressWarnings("serial")
@Entity
public class Contact implements Serializable{
	
	@GeneratedValue
	@Id 
	private Long id;
	private String firstname;
	private String lastname;
	private String dateNaissance;
	private String email;
	private long tel;
	private String photo;
	
	//Constructors
	public Contact() {
		// TODO Auto-generated constructor stub
	}

	public Contact( String firstname, String lastname, String dateNaissance, String email, long tel,
			String photo) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.dateNaissance = dateNaissance;
		this.email = email;
		this.tel = tel;
		this.photo = photo;
	}

	//Getters and setters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getTel() {
		return tel;
	}

	public void setTel(long tel) {
		this.tel = tel;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	
	
}
