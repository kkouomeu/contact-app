import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Contact} from "../model/model.contact";

@Injectable()
export class ContactService{

    constructor(public http:Http){

    }

    getContacts(motCle:string, page:number, size:number){
        return  this.http.get("http://localhost:9000/findContacts?keyword="+motCle+"&page="+page+"&size="+size+"")
            .map(resp=>resp.json());
    }

    getOneContact(id:number){
        return  this.http.get("http://localhost:9000/contacts/"+id)
            .map(resp=>resp.json());
    }

    saveContacts(contact:Contact){
        return this.http.post("http://localhost:9000/contacts",contact)
            .map(resp=>resp.json());
    }

    updateContact(contact:Contact){
        return this.http.put("http://localhost:9000/contacts/"+contact.id,contact)
            .map(resp=>resp.json());
    }
}