import { Component, OnInit } from '@angular/core';
import {Http} from "@angular/http";
import "rxjs/add/operator/map"
import {ContactService} from "../../services/contact.service";
import * as $ from 'jquery';
import {Router} from "@angular/router";

@Component({
  selector: 'app-c',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {

  constructor(public http:Http,
              public contactService:ContactService,
              public router:Router) { }

  //List of contats
  pageContacts:any;
  datas:any;
  keyWord:string = "";
  currentPage:number = 0;
  totalContacts:number = 0;
  size:number = 7;
  pagesArray:Array<number>;



  //Execute everytime the component load
  ngOnInit() {
      this.doSearch();
      //this.doCountContacts();
  }

  doSearch(){
      console.log("component initialization...");
      console.log(this.keyWord);

      this.contactService.getContacts(this.keyWord,this.currentPage,this.size)
          .subscribe(data=>{
              this.pageContacts = data;
              this.datas = this.pageContacts.content;
              this.totalContacts = this.pageContacts.totalElements;
              this.pagesArray = new Array(data.totalPages);

              console.log(data);
              console.log("Total contacts > "+ this.totalContacts + " | Type : "+ typeof this.totalContacts);

          },err=>{
              console.warn(err);
          })
  }

  //Search one contact
  findContact(){
    this.doSearch();
  }


  //Pagination
  gotoPage(i:number){

    this.currentPage = i;
    this.doSearch();
  }

  //Count number of contacts
    /* doCountContacts(){
        $('#count').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).val()
            }, {
                duration: 4000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    } */


    updateContact(id:number){
        this.router.navigate(['edit-contact',id]);
    }




}
