import { Component, OnInit } from '@angular/core';
import {Contact} from "../../model/model.contact";
import {ContactService} from "../../services/contact.service";

@Component({
  selector: 'app-new-contact',
  templateUrl: './new-contact.component.html',
  styleUrls: ['./new-contact.component.css']
})
export class NewContactComponent implements OnInit {

  //To instanciate new model class never forget to call **new**
  contact:Contact = new Contact();

  mode:number = 1;

  constructor(public contactService:ContactService){};

  ngOnInit() {
  }

  saveContact(){

    this.contactService.saveContacts(this.contact)
        .subscribe(data=>{
          this.contact = data;
          console.log(this.contact)
        }, err=>{
          console.log(err)
        })
  }



}
