import { Component, OnInit } from '@angular/core';
import {Contact} from "../../model/model.contact";
import {ActivatedRoute, Router} from "@angular/router";
import {ContactService} from "../../services/contact.service";

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.css']
})
export class EditContactComponent implements OnInit {


    mode:number = 1;
    idContact:number;
    updateCheck:number = 0;

    /* Never forget to initialize with "new" */
    contact : Contact = new Contact();

  constructor(public activedRoute:ActivatedRoute,
              public contactService:ContactService,
              public router:Router) {

    console.log("==================================");
    console.log(activedRoute.snapshot.params['id']);
    console.log("==================================");

    /* Define id of contact to edit with parameter of request  */
    this.idContact = activedRoute.snapshot.params['id'];

  }

    /* Initialize data into fields when component has to load */
  ngOnInit() {
    this.contactService.getOneContact(this.idContact).
        subscribe(data=>{
          this.contact = data
    },err=>{
          console.log(err);
    })
  }


  updateContact(){
    this.contactService.updateContact(this.contact)
        .subscribe(data=>{
            console.log("mise à jour éffectuée");
            this.updateCheck = 1;
            setTimeout(this.clearInterval,4500);


        },err=>{

        })
  }

  clearInterval(){
      var element = document.getElementsByClassName("alert-success")[0];
      element.remove();
      this.updateCheck = 0;
      
  }


}
