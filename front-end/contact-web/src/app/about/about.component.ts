import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }


    infos = {
        name : 'Arsene Kevin',
        email : 'my-favourite-fake_email@fakker.com',
        description : 'Learn Springboot and Angular 4 via project 📆',
        date : '30-09-2018'
    };

}
